const path = require('path');
const fse = require('fs-extra');

const prod = 'dist';
const dev = 'app';
const host = 'binaries/win32-x64';
const prodApp = path.join(prod, 'resources/app');
const prodFiles = ['main.js', 'index.html'];

function packageDist() {
  // delete any previous distribution
  fse.rmSync(prod, {recursive: true, force: true});
  // copy over the Electron binary
  fse.copySync(host, prod);
  // copy over the app files needed for dist
  for (const file of prodFiles) {
    fse.copySync(path.join(dev, file), path.join(prodApp, file));
  }
}

module.exports = function(env, argv) {
  if (env.production) {
    packageDist();
  }

  return {
    entry: './src/renderer.ts',
    mode: env.production ? 'production' : 'development',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          include: path.resolve(__dirname, 'src'),
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
      filename: 'renderer.js',
      path: path.resolve(__dirname, env.production ? prodApp : dev),
    },
    devtool: env.production ? false : 'source-map',
  };
};
