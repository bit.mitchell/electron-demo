
# Super simplified Electron/TypeScript/Phaser demo application.

## Usage

All setup for VSCode to work with. Make sure to grab the recommended extensions.

Three npm scripts exist:

`build` will compile the renderer source code and place it in app, ready to be launched.
`watch` will use webpacks watch feature to incrementally compile the source as changes are made. This is the default build tool.
`package` will perform a production build and package electron plus the application in the dist directory.

Workflow:

Upon opening VSCode, hit Ctrl-Shift-B to launch the watcher build. This should be left in the background while doing work.
Once ready to launch the app, hit F5 to launch and automatically attach to the Electron application.
After changes have been made to the source code, save it and it will automatically be recompiled, then hit Ctrl-Shift-F5 to relaunch the app.
