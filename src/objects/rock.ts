import {GameObjects} from 'phaser';

export class Rock extends GameObjects.Graphics {
  public constructor(scene: Phaser.Scene, x: number = 0, y: number = 0) {
    super(scene);

    this.lineStyle(2, 0xffffff, 1 );
    this.strokeCircle(0, 0, 40);
    this.setPosition(x, y);
  }
}
