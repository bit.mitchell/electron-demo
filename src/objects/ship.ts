import {GameObjects, Physics} from 'phaser';

export class Ship extends GameObjects.Graphics {
  arcadeBody: Physics.Arcade.Body;

  public constructor(scene: Phaser.Scene, x: number = 0, y: number = 0) {
    super(scene);

    scene.physics.add.existing(this);
    this.arcadeBody = this.body as Phaser.Physics.Arcade.Body;

    this.lineStyle(2, 0xffffff, 1 );
    this.beginPath();

    this.moveTo(20, 0);
    this.lineTo(-20, 14);
    this.lineTo(-12, 0);
    this.lineTo(-20, -14);
    this.closePath();
    this.strokePath();

    this.generateTexture('ship');

    this.setPosition(x, y);
    this.arcadeBody.setCircle(17, -17, -17);
    this.arcadeBody.setMass(1);
  }

  public handleInputs(turn: number, thrust: number) {
    this.rotation += turn;

    const dir = this.getDirection();
    this.arcadeBody.setAcceleration(dir.x * thrust, dir.y * thrust);

    this.arcadeBody.velocity.x *= 0.993;
    this.arcadeBody.velocity.y *= 0.993;
  }

  public getDirection(): Phaser.Math.Vector2 {
    return new Phaser.Math.Vector2(
        Math.cos(this.rotation),
        Math.sin(this.rotation));
  }
}
