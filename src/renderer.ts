import {Game} from 'phaser';
import {PlayScene} from './scenes/play';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  width: 1280,
  height: 720,
  parent: 'game',
  scene: PlayScene,
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
    },
  },
};

const game = new Game(config);
