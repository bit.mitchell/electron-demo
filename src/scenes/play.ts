import {Physics, Scene} from 'phaser';
import {Ship} from '../objects/ship';
import {Rock} from '../objects/rock';

export class PlayScene extends Scene {
  updateTime: number = 0;
  updateAccumulator: number = 0;
  updatePeriod: number = 1000 / 60;

  player: Ship;
  rocks: Rock[] = [];

  thrust: Phaser.Input.Keyboard.Key;
  left: Phaser.Input.Keyboard.Key;
  right: Phaser.Input.Keyboard.Key;

  public constructor() {
    super({key: 'play'});
  }

  public preload() {

  }

  public create() {
    const {width, height} = this.game.canvas;

    this.player = this.add.existing(new Ship(this, width / 2, height / 2));

    for (let i = 0; i < 10; i++) {
      const x = Math.random() * width;
      const y = Math.random() * height;
      this.rocks.push(this.add.existing(new Rock(this, x, y)));
    }

    this.thrust = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    this.left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    this.right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  }

  public override update(time: number, delta: number) {
    this.updateAccumulator += delta;
    while (this.updateAccumulator >= this.updatePeriod) {
      this.fixedUpdate(this.updateTime, this.updatePeriod);
      this.updateAccumulator -= this.updatePeriod;
      this.updateTime += this.updatePeriod;
    }
    console.log(`PlayScene.update(); // time = ${time}`);
  }

  public fixedUpdate(time: number, delta: number) {
    // apply thrust to the player
    let spin = 0;

    if (this.left.isDown) {
      spin -= 1;
    }
    if (this.right.isDown) {
      spin += 1;
    }

    this.player.handleInputs(
        spin * delta * 0.004,
        this.thrust.isDown ? 100 : 0);
  }
}
